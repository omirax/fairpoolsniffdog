﻿. .\Include.ps1

$Name = Get-Item $MyInvocation.MyCommand.Path | Select-Object -ExpandProperty BaseName 
 
 
$Fairpool_Request = [PSCustomObject]@{} 
 
 
 try { 
     $Fairpool_Request = Invoke-RestMethod "https://fairpool.pro/api/status" -UseBasicParsing -TimeoutSec 10 -ErrorAction Stop
     #$ZpoolCoins_Request = Invoke-RestMethod "https://fairpool.pro/api/currencies" -UseBasicParsing -TimeoutSec 10 -ErrorAction Stop 
 } 
 catch { 
     Write-Warning "Sniffdog howled at ($Name) for a failed API check. " 
     return 
 } 
 
 if (($Fairpool_Request | Get-Member -MemberType NoteProperty -ErrorAction Ignore | Measure-Object Name).Count -le 1) { 
     Write-Warning "SniffDog sniffed near ($Name) but ($Name) Pool API had no scent. " 
     return
 }     

 $Locations = 'Europe', 'US'
 if ($Locations = 'Europe') {$Fairpool_Host_stratum = "eu1.fairpool.pro"} elseif ($Locations = 'US') {$Fairpool_Host_stratum = "us1.fairpool.pro"}

$Fairpool_Request | Get-Member -MemberType NoteProperty -ErrorAction Ignore | Select -ExpandProperty Name | foreach {
#$Fairpool_Request | Get-Member -MemberType NoteProperty -ErrorAction Ignore | Select-Object -ExpandProperty Name | Where-Object {$Fairpool_Request.$_.hashrate -gt 0} | foreach {
    $Fairpool_Host = $Fairpool_Host_stratum
    $Fairpool_Port = $Fairpool_Request.$_.port
    $Fairpool_Algorithm = Get-Algorithm $Fairpool_Request.$_.name
    $Fairpool_Coin = $Fairpool_Request.$_.coins
    $Fairpool_Fees = $Fairpool_Request.$_.fees
    $Fairpool_Workers = $Fairpool_Request.$_.workers

    $Divisor = 1000000
	
    switch($Fairpool_Algorithm)
    {
        "equihash"{$Divisor /= 1000}
        "blake2s"{$Divisor *= 1000}
	"blakecoin"{$Divisor *= 1000}
        "decred"{$Divisor *= 1000}
	"x11"{$Divisor *= 100}
	"keccak"{$Divisor *= 1000}
	"keccakc"{$Divisor *= 1000}
    }

    if((Get-Stat -Name "$($Name)_$($Fairpool_Algorithm)_Profit") -eq $null){$Stat = Set-Stat -Name "$($Name)_$($Fairpool_Algorithm)_Profit" -Value ([Double]$Fairpool_Request.$_.estimate_last24h/$Divisor*(1-($Fairpool_Request.$_.fees/100)))}
    else{$Stat = Set-Stat -Name "$($Name)_$($Fairpool_Algorithm)_Profit" -Value ([Double]$Fairpool_Request.$_.estimate_current/$Divisor *(1-($Fairpool_Request.$_.fees/100)))}
	
    if($Wallet)
    {
        [PSCustomObject]@{
            Algorithm = $Fairpool_Algorithm
            Info = "$Fairpool_Coin - Coin(s)"
            Price = $Stat.Live
            Fees = $Fairpool_Fees
            StablePrice = $Stat.Week
	    Workers = $Fairpool_Workers
            MarginOfError = $Stat.Fluctuation
            Protocol = "stratum+tcp"
            Host = $Fairpool_Host
            Port = $Fairpool_Port
            User = $Wallet
            Pass = "ID=$RigName,c=$Passwordcurrency"
            Location = $Location
            SSL = $false
        }
    }
}
